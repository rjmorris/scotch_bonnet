# First-time setup

The following steps were used to create the Vue project. (This is for reference only. These steps shouldn't need to be repeated.)

1. `$ dc build`
2. `$ dc run --rm frontend vue create --no-git .`
3. Choose the following options in the `vue create` wizard:

    Select a preset:
      <manual>
    Check the features needed:
      <Babel> <CSS Pre-processors> <Linter/Formatter>
    Pick a CSS pre-processor:
      Sass/SCSS
    Pick a linter/formatter config:
      ESLint+Prettier
    Pick additional lint features:
      <Lint on save>
    Where do you prefer placing config for Babel, etc.?
      dedicated config files
    Save as preset?
      no
    Pick the package manager:
      yarn
