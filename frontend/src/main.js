import Vue from "vue";
import VueRouter from "vue-router";
import { BootstrapVue } from "bootstrap-vue";
import App from "./App.vue";
import PriceList from "./components/PriceList.vue";

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);

const router = new VueRouter({
  routes: [
    {
      name: "root",
      path: "",
      component: PriceList,
    },
  ],
});

new Vue({
  router,
  render: h => h(App),
}).$mount("#app");
