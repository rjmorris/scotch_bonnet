export function formatDollar(str, undefStr) {
  if (str === null || str === undefined) {
    return undefStr;
  }
  return str.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });
}

export function makeMonthField(date) {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  if (month <= 9) {
    return `${year}_0${month}`;
  }
  return `${year}_${month}`;
}
