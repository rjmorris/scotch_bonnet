// Bootstrap defaults (from node_modules/bootstrap/scss/_variables.scss):

// $blue:   #007bff
// $indigo: #6610f2
// $purple: #6f42c1
// $pink:   #e83e8c
// $red:    #dc3545
// $orange: #fd7e14
// $yellow: #ffc107
// $green:  #28a745
// $teal:   #20c997
// $cyan:   #17a2b8

// $gray-100: #f8f9fa
// $gray-600: #6c757d
// $gray-800: #343a40

// $primary:   $blue
// $secondary: $gray-600
// $success:   $green
// $info:      $cyan
// $warning:   $yellow
// $danger:    $red
// $light:     $gray-100
// $dark:      $gray-800

$copper: #ce6c1a;
$green: #15a515;
$yellow: #ffedb8;
$blue: #0033cc;
$red: #cc0033;

$primary: $copper;

@import 'node_modules/bootstrap/scss/bootstrap.scss';
@import 'node_modules/bootstrap-vue/src/index.scss';

// Change the table header styling to make it more obvious which cell the sort
// icon belongs to. Otherwise, when the text is left justified and the icon is
// right justified in a wide cell, the icon looks like it belongs to the next
// cell.

.table.b-table > thead > tr > [role=columnheader] {
  border-top: none;
  border-bottom: none;
  border-left: solid 1px white;
  border-right: solid 1px white;
  background-color: rgba(0, 0, 0, 0.075);
}

// Override the calculation BootstrapVue uses for the sort icon's position in
// table headers. It centers the icon vertically, which is fine when none of the
// header cells contain wrapped text. But when one cell does contain wrapped
// text, the icon appears higher than the text in the other cells (the text is
// bottom aligned).
//
// This new calculation bottom aligns the icon, accounting for the padding and a
// fudge factor to make it look right.

.table.b-table > thead > tr > [aria-sort]:not(.b-table-sort-icon-left) {
  background-position: right 0.3rem top calc(100% - 0.75rem - 0.4rem);
}


// I set up the price list table to display in stacked mode at the xs and sm
// breakpoints. The default styling in stacked mode made it hard to see the
// boundaries between rows. This styling fixes that by making each row look more
// like a card.

@include media-breakpoint-down(sm) {
  .table.b-table.b-table-stacked-md > tbody > tr {
    margin-bottom: 1rem;
    border: solid 1px #dddddd;
    border-radius: 0.25rem;
  }
}
