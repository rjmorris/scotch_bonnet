# Scotch Bonnet

The Scotch Bonnet is a web application displaying current and past prices of retail bottles of liquor sold by North Carolina ABC stores. The prices are published on the NC ABC website, but this web application is intended to provide a nicer interface.

## Quick start

Run the following to bring up a development version of the site:

```
$ docker-compose build
$ docker-compose run --rm pipeline
$ docker-compose up
```

The step that runs the pipeline may take a few minutes on a fresh checkout.

Then visit http://localhost:8080.

## Data pipeline

The data pipeline collects the price lists published by the ABC Commission and transforms them into data files used by the web application.

The data pipeline is implemented in Python using Docker. Build the Docker image with:

```
$ docker-compose build pipeline
```

### Workflows

The data pipeline is split into the following workflows:

- Downloading and configuring the price lists.
- Building the web application input files.

Downloaded price lists are committed to the repo, so if all you're doing is bringing up a development version of the site, you can skip that workflow. You will need to build the web application input files before starting the web application, though.

#### Downloading and configuring the price lists

The ABC Commission releases two price lists of interest:

- the full price for all products, released quarterly (Feb, May, Aug, Nov)
- the sale price for a subset of products, released monthly

The steps required to download and configure a new price list differ between the full and sale price lists.

##### Sale price list

The sale price list is easier, so we'll start there. When a new sale price list has been released, run the following command to download it:

```
$ docker-compose run --rm pipeline python flow.py download --sale YYYY_MM
```

replacing `YYYY_MM` with the desired year/month.

##### Full price list

The full price list is available in two formats: a PDF file and a CSV file. Each format has its benefits:

- The PDF feels more permanent and official. It has an effective date printed in it, and it exists at a URL you can revisit months later. The CSV is an export from an HTML page, and there isn't a way to go back and get old versions.
- The PDF is usually available a few days before the start of the next quarter. This gives me some time to have things ready on the first day of the quarter. The CSV is "live", so it isn't available for a new quarter until the first day of that quarter.
- The PDF includes aisle designations, and the CSV doesn't. (However, I'm not currently using the aisle info from the PDF, so this is only a theoretical benefit for now.)
- The CSV might be updated mid-quarter to reflect changes such as new products being added or names being tweaked. The PDF doesn't get those updates.
- The CSV is vastly easier to parse. However, the parsing code for the PDF is already written, so this won't become a benefit until the layout of the PDF changes. (It hasn't yet, but it seems probable that it will at some point.) Related, the PDF requires manual steps each time to identify page coordinates where the data ends.
- The URL for the CSV is consistent, so it's easy to download it programatically. The URL for the PDF is different every quarter and doesn't necessarily follow a consistent pattern.

The pipeline supports either PDF or CSV, so you can choose whichever is more convenient at the time.

When you're ready to download a full price list, first decide if you want the PDF or the CSV:

- In general, prefer the PDF if it's available.
- If you want the price list for an upcoming month or an older month, the PDF is your only choice. The CSV is available only for the current month.

If you want the PDF, determine its URL and add it to [./pipeline/urls.yml](./pipeline/urls.yml). Instructions can be found in that file.

Next, download the file with:

```
$ docker-compose run --rm pipeline python flow.py download --full YYYY_MM
```

replacing `YYYY_MM` with the desired year/month. The file will be downloaded to `./data/downloaded/full/YYYY_MM.pdf` or `./data/downloaded/fullcsv/YYYY_MM.csv`.

The remaining steps in this section are required only for PDF files. If you downloaded a CSV file, you can skip to the next section.

Open the downloaded PDF file and find the last page of active products. This will be the page where the "Discontinued Items" section starts, or the page before that if "Discontinued Items" starts at the top of a new page. This should be near the end of the document. Verify that there are no other sections after "Discontinued Items", because this is an assumption made by the pipeline.

Next, determine the y-coordinate of the bottom of the bounding box surrounding the active products on the last page of active products. The script [./pipeline/plot_page.py](./pipeline/plot_page.py) can help you find this by displaying a window showing where text was found on the page and the coordinates of your mouse as you move it around. Unfortunately, this script doesn't run properly inside a Docker container, because we haven't set up Docker to create graphical windows, so you'll first need to install a virtual environment on your host. After you have activated your virtual environment, run the script as:

```
$ cd pipeline
$ python plot_page.py ../data/downloaded/full/YYYY_MM.pdf PP
```

replacing `YYYY_MM` with the desired year/month and `PP` with the page number of the last page of active products.

Finally, add an entry to [./pipeline/table_coords.yml](./pipeline/table_coords.yml) specifying the last page of active products and the y-coordinate of the last product.

#### Building the web application input files

Once all the price lists have been downloaded, you're ready to build the web application input files.

The web application displays past, present, and future prices. You must configure the target month and the range of months to include in the data by editing [./pipeline/run_env.sh](./pipeline/run_env.sh).

Then build the web application's input files with:

```
$ docker-compose run --rm pipeline
```

If everything completes successfully, the files will be stored in [./data/dist/](./data/dist).

If this is the first time you've run the command since you downloaded a new quarterly full price list, you'll likely get an error indicating that some products are missing the "aisle" attribute. An aisle (like "Rum" or "Vodka") is assigned to each product manually, with the assignments stored in [./pipeline/aisles.csv](./pipeline/aisles.csv). Update that file by adding a row for each product listed in [./pipeline/missing_aisles.csv](./pipeline/missing_aisles.csv). To decide which aisle to assign to a product, look up the product in the downloaded price list, find another product in its same section, and see which aisle we've assigned to that other product.

### Dependencies

Python dependencies are managed using [pip-tools](https://github.com/jazzband/pip-tools). If you need to install a new package in the Docker image:

1. Insert the new package into `pipeline/requirements.in`.
1. Run `docker-compose run --rm pipeline pip-compile` to generate a new version of `pipeline/requirements.txt` reflecting the newly installed package.
1. Run `docker-compose build pipeline` to rebuild the image so that it contains the new package.

To upgrade a package in the Docker image:

1. Run `docker-compose run --rm pipeline pip-compile --upgrade-package <pkgname>` to generate a new version of `pipeline/requirements.txt` reflecting the upgraded package.
1. Run `docker-compose build pipeline` to rebuild the image so that it contains the upgraded package.

If you want to use a virtual environment on the host in addition to the one provided by Docker, the workflow on the host is a little different. First create and activate the virtual environment. Install the dependencies the first time using `pip install -r requirements.txt`, which will install `pip-tools` along with the other dependencies. To install a new package in your host's virtual environment:

1. Change into the `pipeline` directory.
1. Insert the new package into `requirements.in`.
1. Run `pip-compile` to generate a new version of `requirements.txt` reflecting the newly installed package.
1. Run `pip-sync` to apply any changes to your virtual environment.

To upgrade a package in your host's virtual environment:

1. Change into the `pipeline` directory.
1. Run `pip-compile --upgrade-package <pkgname>` to generate a new version of `requirements.txt` reflecting the upgraded package.
1. Run `pip-sync` to apply any changes to your virtual environment.

## Web application

The web application is implemented in Vue using Docker. To run the development version of the application:

```
$ docker-compose build frontend
$ docker-compose up
```

The application will then be available at `http://localhost:8080`. To run it on a different port, do this instead:

```
$ docker-compose build frontend
$ export FRONTEND_PORT=8401
$ docker-compose up
```

To install or upgrade a package:

1. Run `docker-compose run --rm frontend yarn add <pkgname>` to install the package in a container. This will update `package.json` and `yarn.lock`. Replace `yarn add` with `yarn upgrade` to upgrade an existing package.
1. Run `docker-compose build frontend` to rebuild the image so that it contains the new package.

The `node_modules` directory inside the image won't be available on the host. If you need to install packages on the host (for dev tooling, for example), you'll need to run `yarn install` on the host.
