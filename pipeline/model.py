from dataclasses import dataclass
from datetime import date
from enum import Enum
from functools import total_ordering
import re
from typing import Dict, Sequence, Union


class DataType(Enum):
    PDF = "pdf"
    CSV = "csv"


@total_ordering
class Month:
    def __init__(self, year: int, month: int):
        self.year = year
        self.month = month

    def next(self) -> "Month":
        next_month = self.month + 1
        next_year = self.year
        if next_month == 13:
            next_month = 1
            next_year += 1
        return Month(year=next_year, month=next_month)

    def prev(self) -> "Month":
        prev_month = self.month - 1
        prev_year = self.year
        if prev_month == 0:
            prev_month = 12
            prev_year -= 1
        return Month(year=prev_year, month=prev_month)

    @classmethod
    def from_today(cls) -> "Month":
        today = date.today()
        return cls(year=today.year, month=today.month)

    @classmethod
    def from_string(cls, string: str) -> "Month":
        year, month = re.split("[-_]", string)
        return cls(year=int(year), month=int(month))

    def __eq__(self, other: "Month") -> bool:
        if not isinstance(other, Month):
            return False
        return self.year == other.year and self.month == other.month

    def __hash__(self) -> int:
        return hash((self.year, self.month))

    def __lt__(self, other: "Month") -> bool:
        if not isinstance(other, Month):
            raise TypeError(f"Cannot compare objects of type {type(other)} and {type(self)}")
        return (self.year, self.month) < (other.year, other.month)

    def __str__(self) -> str:
        return f"{self.year}_{self.month:02}"

    def __repr__(self) -> str:
        return f"Month({self.year}, {self.month})"


Url = str


@dataclass
class UrlConfig:
    full_pdf: Dict[Month, Url]
    full_csv: str
    sale_template: str

    def has_full_pdf_url(self, month: Month) -> bool:
        return month in self.full_pdf

    def full_pdf_url(self, month: Month) -> Url:
        return self.full_pdf[month]

    def sale_url(self, month: Month) -> Url:
        return self.sale_template.format(
            year=month.year,
            month=month.month,
        )


@dataclass
class PageTableCoords:
    left: int
    top: int
    right: int
    bottom: int
    columns: Sequence[int]


@dataclass
class PageRange:
    start: int
    end: int

    def __eq__(self, other: "PageRange") -> bool:
        if not isinstance(other, PageRange):
            return False
        return self.start == other.start and self.end == other.end

    def __hash__(self):
        return hash((self.start, self.end))


DocumentTableCoords = Dict[PageRange, PageTableCoords]


TableCoordsConfig = Dict[Month, DocumentTableCoords]


AisleConfig = Dict[str, str]
