#!/bin/bash

source run_env.sh

python flow.py \
       build \
       $TARGET_MONTH \
       --months-before $MONTHS_BEFORE \
       --months-after $MONTHS_AFTER \
       --data-dir /data \
       --products-file /data/dist/products.json \
       --download-time-file /data/dist/download_time.json
