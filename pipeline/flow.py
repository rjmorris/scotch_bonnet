import argparse
import logging
from pathlib import Path

import tasks as t
from model import DataType, Month


logger = logging.getLogger("scotch bonnet")
logger.setLevel(logging.INFO)
log_handler = logging.StreamHandler()
log_handler.setLevel(logging.INFO)
log_formatter = logging.Formatter(
    fmt="{asctime} {levelname}: {message}",
    style="{",
    datefmt="%Y-%m-%d %I:%M:%S %p",
)
log_handler.setFormatter(log_formatter)
logger.addHandler(log_handler)


parser = argparse.ArgumentParser(
    description="Build the product data file that is imported by the web app",
)
subparsers = parser.add_subparsers(required=True, dest="command")
download_parser = subparsers.add_parser("download", help="Download price lists")
build_parser = subparsers.add_parser("build", help="Build the product data file")

download_parser.add_argument(
    "--full",
    type=Month.from_string,
    nargs="+",
    help="Download the full price list for the given months "
    "(express months as YYYY_MM)",
    metavar="MONTH",
)
download_parser.add_argument(
    "--sale",
    type=Month.from_string,
    nargs="+",
    help="Download the sale price list for the given months "
    "(express months as YYYY_MM)",
    metavar="MONTH",
)
download_parser.add_argument(
    "--url-config-file",
    type=Path,
    default="urls.yml",
)
download_parser.add_argument(
    "--data-dir",
    type=Path,
    default="../data",
)

build_parser.add_argument(
    "target_month",
    type=Month.from_string,
    help="Month to create data for (express as YYYY_MM)",
)
build_parser.add_argument(
    "--months-before",
    type=int,
    default=12,
    help="Number of months of data before the target month to include "
    "Default: %(default)s",
)
build_parser.add_argument(
    "--months-after",
    type=int,
    default=1,
    help="Number of months of data after the target month to include "
    "Default: %(default)s",
)
build_parser.add_argument(
    "--url-config-file",
    type=Path,
    default="urls.yml",
)
build_parser.add_argument(
    "--table-coords-config-file",
    type=Path,
    default="table_coords.yml",
)
build_parser.add_argument(
    "--aisle-config-file",
    type=Path,
    default="aisles.csv",
)
build_parser.add_argument(
    "--missing-aisle-file",
    type=Path,
    default="missing_aisles.csv",
)
build_parser.add_argument(
    "--data-dir",
    type=Path,
    default="../data",
)
build_parser.add_argument(
    "--products-file",
    type=Path,
    default="../data/dist/products.json",
)
build_parser.add_argument(
    "--download-time-file",
    type=Path,
    default="../data/dist/download_time.json",
)
args = parser.parse_args()

if args.command == "download":
    if args.full is None:
        args.full = []
    if args.sale is None:
        args.sale = []

    url_config = t.import_url_config(args.url_config_file)

    for month in args.full:

        if (t.has_remote_url_full_pdf(month, url_config)):
            remote_url = t.get_remote_url_full_pdf(month, url_config)
            downloaded_path = t.get_downloaded_path_full_pdf(args.data_dir, month)
            t.download_data(
                url=remote_url,
                to_path=downloaded_path,
            )
            download_time_path = t.get_download_time_path_full_pdf(args.data_dir, month)
        elif month == Month.from_today():
            remote_url = t.get_remote_url_full_csv(url_config)
            downloaded_path = t.get_downloaded_path_full_csv(args.data_dir, month)
            t.download_full_csv(
                url=remote_url,
                to_path=downloaded_path,
            )
            download_time_path = t.get_download_time_path_full_csv(args.data_dir, month)
        else:
            raise ValueError(
                f"Cannot download full data for {month}. "
                "The requested month must either be the current month "
                "or have its PDF URL defined in the URL config file."
            )

        download_time = t.get_current_time()
        t.write_timestamp(download_time, download_time_path)

    for month in args.sale:
        downloaded_path = t.get_downloaded_path_sale(args.data_dir, month)
        t.download_data(
            url=t.get_remote_url_sale(month, url_config),
            to_path=downloaded_path,
        )
        download_time = t.get_current_time()
        download_time_path = t.get_download_time_path_sale(args.data_dir, month)
        t.write_timestamp(download_time, download_time_path)

elif args.command == "build":
    all_months = t.get_all_months(args.target_month, args.months_before, args.months_after)

    url_config = t.import_url_config(args.url_config_file)
    table_coords_config = t.import_table_coords_config(args.table_coords_config_file)
    aisle_config = t.import_aisle_config(args.aisle_config_file)

    download_times = []

    dfs_full = {}

    for month in all_months:
        data_month, data_type = t.find_most_recent_full_data(args.data_dir, month)

        if data_type == DataType.PDF:
            downloaded_path = t.get_downloaded_path_full_pdf(args.data_dir, data_month)
            download_time_path = t.get_download_time_path_full_pdf(args.data_dir, data_month)
        else:
            downloaded_path = t.get_downloaded_path_full_csv(args.data_dir, data_month)
            download_time_path = t.get_download_time_path_full_csv(args.data_dir, data_month)
        download_time = t.read_timestamp(download_time_path)

        parsed_path = t.get_parsed_path_full(args.data_dir, data_month)
        parse_time_path = t.get_parse_time_path_full(args.data_dir, data_month)

        if (
            t.path_exists(parsed_path)
            and t.path_exists(parse_time_path)
            and t.is_greater_than(t.read_timestamp(parse_time_path), download_time)
        ):
            dfs_full[month] = t.load_df_from_csv(parsed_path)
        else:
            if data_type == DataType.PDF:
                table_coords = t.get_table_coords(data_month, table_coords_config)
                dfs_full[month] = t.parse_downloaded_full_pdf(
                    from_path=downloaded_path,
                    table_coords=table_coords,
                )
            else:
                dfs_full[month] = t.parse_downloaded_full_csv(
                    from_path=downloaded_path,
                )
            t.save_df_to_csv(dfs_full[month], parsed_path)
            parse_time = t.get_current_time()
            t.write_timestamp(parse_time, parse_time_path)

        dfs_full[month] = t.keep_columns(
            dfs_full[month],
            ["code", "brand", "proof", "size", "retail_price"],
        )
        dfs_full[month] = t.dedupe(dfs_full[month], cols=["code"])

        download_times.append(download_time)

    dfs_sale = {}

    for month in all_months:
        downloaded_path = t.get_downloaded_path_sale(args.data_dir, month)
        download_time_path = t.get_download_time_path_sale(args.data_dir, month)
        download_time = t.read_timestamp(download_time_path)

        parsed_path = t.get_parsed_path_sale(args.data_dir, month)
        parse_time_path = t.get_parse_time_path_sale(args.data_dir, month)

        if (
            t.path_exists(parsed_path)
            and t.path_exists(parse_time_path)
            and t.is_greater_than(t.read_timestamp(parse_time_path), download_time)
        ):
            dfs_sale[month] = t.load_df_from_csv(parsed_path)
        else:
            dfs_sale[month] = t.parse_downloaded_sale(
                from_path=downloaded_path,
            )
            t.save_df_to_csv(dfs_sale[month], parsed_path)
            parse_time = t.get_current_time()
            t.write_timestamp(parse_time, parse_time_path)

        dfs_sale[month] = t.keep_columns(dfs_sale[month], ["code", "sale_price"])
        dfs_sale[month] = t.dedupe(dfs_sale[month], cols=["code"])

        download_times.append(download_time)


    dfs_joined = [
        t.join_full_sale(
            full=dfs_full[month],
            sale=dfs_sale[month],
        )
        for month in all_months
    ]

    dfs_joined = [
        t.assign_month(df, month)
        for df, month in zip(dfs_joined, all_months)
    ]

    df = t.concat_monthly_data(dfs_joined)


    target_products = t.find_target_products(dfs_full, args.target_month, args.months_after)
    df = t.subset_target_products(df, target_products)
    df = t.compute_price(df)
    df = t.drop_columns(df, ["retail_price", "sale_price"])
    df = t.pivot_by_product(df)
    df = t.drop_columns(df, ["month", "price"])
    df = t.normalize_volume(df)
    df = t.assign_aisle(df, aisle_config)
    t.check_missing_aisle(df, args.missing_aisle_file)
    df = t.sort(df, ["brand"])

    t.save_df_to_json(df, args.products_file)

    latest_download_time = t.find_max(download_times)
    t.write_timestamp(timestamp=latest_download_time, to_path=args.download_time_file)
