import argparse
from pathlib import Path

import camelot
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument("file", type=Path)
parser.add_argument("page")
args = parser.parse_args()

tables = camelot.read_pdf(
    str(args.file),
    flavor="stream",
    pages=args.page,
)

if len(tables) >= 1:
    camelot.plot(tables[0])
    plt.show()
else:
    raise ValueError(f"Expected at least 1 table to be parsed from the page but found {len(tables)}")
