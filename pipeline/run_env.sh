# The web application displays 12 months of past prices and 1 month of future
# prices, relative to the current month when the user views the page.
#
# TARGET_MONTH, MONTHS_BEFORE, and MONTHS_AFTER control the following:
#
# - The set of products to include in the data. Any product appearing in a price
#   list for months in the range [TARGET_MONTH, TARGET_MONTH + MONTHS_AFTER]
#   will be included in the data.
# - The range of months to include in the data. For each product included in the
#   data, prices for months in the range [TARGET_MONTH - MONTHS_BEFORE,
#   TARGET_MONTH + MONTHS_AFTER] will be included in the data.
#
# When assigning these variables, the primary question to ask yourself is: "Am I
# planning to run the pipeline again at the beginning of next month?"
#
# - If yes, then you don't have to worry about creating data during this
#   pipeline run that will also work for next month. Assign:
#
#       - TARGET_MONTH=<this_month>
#       - MONTHS_BEFORE=12
#       - MONTHS_AFTER=1
#
# - If no, then the data you create during this pipeline run will need to
#   work for both this month and next month. Assign:
#
#       - TARGET_MONTH=<this_month>
#       - MONTHS_BEFORE=12
#       - MONTHS_AFTER=2
#
#   (If you don't have 2 months of future prices available, then assign
#   MONTHS_AFTER=1 and try to run the pipeline again when that second month of
#   future data is released.)
#
#   An exception to the "no" case is if you're running the pipeline after ABC
#   stores close on the last open day of this month. In that case, it's probably
#   fine to create data that will work for next month but not this month, and
#   you can assign:
#
#       - TARGET_MONTH=<next_month>
#       - MONTHS_BEFORE=12
#       - MONTHS_AFTER=1
#
# Note that TARGET_MONTH must be expressed as YYYY_MM.

export TARGET_MONTH=2025_03
export MONTHS_BEFORE=12
export MONTHS_AFTER=1
