import camelot
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path


pd.set_option("display.max_columns", None)


orig_dir = Path("../data/orig/full")


def plot_page(tbl):
    camelot.plot(tbl)
    plt.show()


page = camelot.read_pdf(
    str(orig_dir / "2021_05.pdf"),
    flavor="stream",
    pages="81",
)

print(len(page))
print(page[0].shape)
print(page[0].df)

plot_page(page[0])
