import csv
from datetime import datetime, timezone
import itertools
import json
import logging
from pathlib import Path
from typing import Dict, List, Optional, Sequence, Tuple, TypeVar, Union

import camelot
import numpy as np
import pandas as pd
import requests
import yaml

from model import (
    AisleConfig,
    DataType,
    DocumentTableCoords,
    Month,
    PageRange,
    PageTableCoords,
    TableCoordsConfig,
    Url,
    UrlConfig,
)


logger = logging.getLogger("scotch bonnet")


def get_all_months(target_month: Month, before: int, after: int) -> List[Month]:
    months = [target_month]

    current_month = target_month
    for i in range(before):
        current_month = current_month.prev()
        months.append(current_month)

    current_month = target_month
    for i in range(after):
        current_month = current_month.next()
        months.append(current_month)

    return sorted(months)


def import_url_config(config_file: Path) -> UrlConfig:
    with open(config_file) as handle:
        obj = yaml.safe_load(handle)

    full_pdf = {
        Month(year=f["year"], month=f["month"]): f["url"]
        for f in obj["full_pdf"]
    }

    full_csv = obj["full_csv"]["url"]

    sale_template = obj["supplemental_xls"]["url_template"]

    return UrlConfig(
        full_pdf=full_pdf,
        full_csv=full_csv,
        sale_template=sale_template,
    )


def has_remote_url_full_pdf(month: Month, config: UrlConfig) -> Url:
    return config.has_full_pdf_url(month)


def get_remote_url_full_pdf(month: Month, config: UrlConfig) -> Url:
    return config.full_pdf_url(month)


def get_remote_url_full_csv(config: UrlConfig) -> Url:
    return config.full_csv


def get_remote_url_sale(month: Month, config: UrlConfig) -> Url:
    return config.sale_url(month)


def find_most_recent_full_data(data_dir: Path, month: Month) -> Tuple[Month, DataType]:
    MAX_TRIES = 1_000
    tries = 0
    try_month = month
    while (tries <= MAX_TRIES):
        try_path = get_downloaded_path_full_pdf(data_dir, try_month)
        if path_exists(try_path):
            return try_month, DataType.PDF
        try_path = get_downloaded_path_full_csv(data_dir, try_month)
        if path_exists(try_path):
            return try_month, DataType.CSV
        try_month = try_month.prev()
        tries += 1
    raise ValueError(f"No full data prior to {month} could be found.")


def get_downloaded_path_full_pdf(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "full" / f"{month}.pdf"


def get_downloaded_path_full_csv(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "fullcsv" / f"{month}.csv"


def get_downloaded_path_sale(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "sale" / f"{month}.xls"


def path_exists(path: Path) -> bool:
    return path.exists()


def download_full_csv(url: Url, to_path: Path) -> None:
    logger.info(f"Downloading {url} to {to_path}")
    # The remote endpoint requires us to POST multipart/form-data, but the data
    # can be empty. We can trick requests into doing this by passing a dict as
    # the files argument. The dict can't be empty, though, or requests will
    # raise an error.
    response = requests.post(url, files=dict(foo=''))
    response.raise_for_status()
    to_path.parent.mkdir(parents=True, exist_ok=True)
    with open("full.csv", mode="w") as handle:
        handle.write(response.text)


def download_data(url: Url, to_path: Path) -> None:
    logger.info(f"Downloading {url} to {to_path}")
    response = requests.get(url)
    response.raise_for_status()
    to_path.parent.mkdir(parents=True, exist_ok=True)
    with open(to_path, mode="wb") as handle:
        handle.write(response.content)


def get_parsed_path_full(data_dir: Path, month: Month) -> Path:
    return data_dir / "parsed" / "full" / f"{month}.csv"


def get_parsed_path_sale(data_dir: Path, month: Month) -> Path:
    return data_dir / "parsed" / "sale" / f"{month}.csv"


def import_table_coords_config(config_file: Path) -> TableCoordsConfig:
    with open(config_file, mode="r") as handle:
        obj = yaml.safe_load(handle)

    config = {}

    top = 571
    top_page1 = 502
    bottom = 48
    left = 16
    right = 772
    columns=(61, 191, 407, 437, 486, 542, 614, 682, 738)

    for document in obj:
        month = Month(year=document["year"], month=document["month"])
        document_coords = {}

        page_range = PageRange(start=1, end=1)
        document_coords[page_range] = PageTableCoords(
            left=left,
            top=top_page1,
            right=right,
            bottom=bottom,
            columns=columns,
        )

        page_range = PageRange(start=2, end=document["last_page"] - 1)
        document_coords[page_range] = PageTableCoords(
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            columns=columns,
        )

        page_range = PageRange(start=document["last_page"], end=document["last_page"])
        document_coords[page_range] = PageTableCoords(
            left=left,
            top=top,
            right=right,
            bottom=document["last_page_bottom"],
            columns=columns,
        )

        config[month] = document_coords

    return config


def get_table_coords(month: Month, config: TableCoordsConfig) -> DocumentTableCoords:
    return config[month]


# utility function, not a task
def is_code(value: pd.Series) -> pd.Series:
    """
    Return a boolean Series indicating the elements that match the pattern of ID
    codes that ABC assigns to the products.
    """
    return value.str.match("\d\d-\d\d\d", na=False)


# utility function, not a task
def camelot_pages(page_range: PageRange) -> str:
    if page_range.start == page_range.end:
        return f"{page_range.start}"
    return f"{page_range.start}-{page_range.end}"


# utility function, not a task
def camelot_table_areas(coords: PageTableCoords) -> str:
    return f"{coords.left},{coords.top},{coords.right},{coords.bottom}"


# utility function, not a task
def camelot_columns(coords: PageTableCoords) -> str:
    return ",".join(str(c) for c in coords.columns)


def parse_downloaded_full_csv(from_path: Path) -> pd.DataFrame:
    logger.info(f"Parsing full list as CSV from {from_path}")

    df = pd.read_csv(from_path, dtype=str)

    df = df.rename(columns={
        "NC Code": "code",
        "Supplier": "supplier",
        "Brand Name": "brand",
        "Age": "age",
        "Proof": "proof",
        "Bottle Size": "size",
        "Retail Bottle Price": "retail_price",
        "Mixed Beverage Price": "mxb_price",
    })

    price_cols = [
        "retail_price",
        "mxb_price",
    ]
    df[price_cols] = df[price_cols].replace(regex=",", value="")
    df[price_cols] = df[price_cols].replace(regex="\$", value="")
    for col in price_cols:
        df[col] = pd.to_numeric(df[col])

    return df


def parse_downloaded_full_pdf(from_path: Path, table_coords: DocumentTableCoords) -> pd.DataFrame:
    logger.info(f"Parsing full list as PDF from {from_path}")

    table_lists = [
        camelot.read_pdf(
            str(from_path),
            flavor="stream",
            pages=camelot_pages(pages),
            table_areas=[camelot_table_areas(coords)],
            columns=[camelot_columns(coords)],
        )
        for pages, coords in table_coords.items()
    ]

    df = pd.concat([
        table.df
        for table in itertools.chain.from_iterable(table_lists)
    ])

    df = df.rename(columns={
        0: "code",
        1: "supplier",
        2: "brand",
        3: "age",
        4: "proof",
        5: "size",
        6: "case_price_less_bailment",
        7: "case_price_with_surcharge",
        8: "retail_price",
        9: "mxb_price",
    })

    df = df.drop(columns=[
        "case_price_less_bailment",
        "case_price_with_surcharge",
    ])

    # The aisle name is displayed in the PDF as a subheader that spans
    # columns. Shorter aisle names get parsed into the "code" column, and
    # longer aisle names get parsed into the "supplier" column.
    #
    # To assign an aisle to each row, first we'll pull either the "code" or
    # "supplier" column into a new "aisle" column. Then we'll fill down to
    # apply the aisle name to the following rows. However, the "code" and
    # "supplier" columns sometimes include other things we don't want (mostly at
    # the top and bottom of each page), so we have to remove those before
    # filling down, or else the fill operation would stop at these other things
    # instead of continuing to extend down to the next aisle name.
    df["aisle"] = ""
    bad_code = ~is_code(df["code"])
    blank_code = df["code"] == ""
    df.loc[bad_code, "aisle"] = df.loc[bad_code, "code"]
    df.loc[blank_code, "aisle"] = df.loc[blank_code, "supplier"]
    df.loc[df["aisle"].isin(["", "CODE"]), "aisle"] = np.nan
    df["aisle"] = df["aisle"].fillna(method="ffill")

    df = df[is_code(df["code"])]

    price_cols = [
        "retail_price",
        "mxb_price",
    ]
    df[price_cols] = df[price_cols].replace(regex=",", value="")
    for col in price_cols:
        df[col] = pd.to_numeric(df[col])

    return df


def parse_downloaded_sale(from_path: Path) -> pd.DataFrame:
    logger.info(f"Parsing sale list {from_path}")

    df_raw = pd.read_excel(from_path)
    if df_raw.shape[0] <= 11:
        # In cases where the downloaded sale file includes no data, its format
        # is different enough that our regular pd.read_excel() call breaks.
        # Instead of trying to find the right set of options to make
        # pd.read_excel() work in both cases, detect the no-data case and return
        # an empty data frame with the right columns.
        df = pd.DataFrame(columns=["code", "brand", "size", "retail_price", "sale_price"])
        return df
    else:
        df = pd.read_excel(
            from_path,
            skiprows=11,
            header=None,
            usecols="B,E,H,R,U",
            names=["code", "brand", "size", "retail_price", "sale_price"],
            dtype=str,
        )
        df = df[is_code(df["code"])]

    price_cols = [
        "retail_price",
        "sale_price",
    ]
    for col in price_cols:
        df[col] = pd.to_numeric(df[col])

    return df


def join_full_sale(full: pd.DataFrame, sale: pd.DataFrame) -> pd.DataFrame:
    return full.merge(sale, on="code", how="left")


def assign_month(df: pd.DataFrame, month: Month) -> pd.DataFrame:
    return df.assign(month=month)


def concat_monthly_data(dfs: List[pd.DataFrame]) -> pd.DataFrame:
    return pd.concat(dfs)


def compute_price(df: pd.DataFrame) -> pd.DataFrame:
    return df.assign(price=df["sale_price"].combine_first(df["retail_price"]))


def find_target_products(dfs: Dict[Month, pd.DataFrame], target_month: Month, after: int) -> List[str]:
    months = set()
    months.add(target_month)
    current_month = target_month
    for i in range(after):
        current_month = current_month.next()
        months.add(current_month)

    products = set()
    for month in months:
        df = dfs[month]
        products.update(df["code"].tolist())

    return list(products)


def subset_target_products(df: pd.DataFrame, target_products: List[str]) -> pd.DataFrame:
    new = df.copy()
    return new[new["code"].isin(target_products)]


def pivot_by_product(df: pd.DataFrame) -> pd.DataFrame:
    df.sort_values(by=['month'])

    prices = df.pivot(index="code", columns="month", values="price")
    prices.columns = [f"price_{m}" for m in prices.columns]

    return (
        df
        .drop_duplicates(subset=["code"], keep="last")
        .set_index("code")
        .join(prices)
        .reset_index()
    )


def import_aisle_config(config_file: Path) -> AisleConfig:
    config = {}
    with open(config_file, mode="r", newline="") as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            config[row["code"]] = row["aisle"]
    return config


def assign_aisle(df: pd.DataFrame, config: AisleConfig) -> pd.DataFrame:
    new = df.copy()
    new["aisle"] = new["code"].map(config)
    return new


def check_missing_aisle(df: pd.DataFrame, to_path: Path) -> None:
    missing_aisle = df[df["aisle"].isnull()]
    if missing_aisle.shape[0] > 0:
        to_path.parent.mkdir(parents=True, exist_ok=True)
        missing_aisle[["code", "brand", "aisle"]].to_csv(to_path, index=False)
        raise ValueError(
            f"Products are missing the 'aisle' attribute. See {to_path} for the list of"
            " these products."
        )


# utility function, not a task
def size_ml(size):
    if size.endswith("ML"):
        ml = int(size[:-2])
    elif size.endswith("L"):
        ml = int(1000 * float(size[:-1]))
    else:
        raise ValueError(f"Size {size} doesn't match an expected pattern")
    return ml


def normalize_volume(df: pd.DataFrame) -> pd.DataFrame:
    new = df.copy()
    new["size"] = new["size"].map(size_ml)
    return new


def simplify_aisle(df: pd.DataFrame) -> pd.DataFrame:
    pass


def keep_columns(df: pd.DataFrame, cols: List[str]) -> pd.DataFrame:
    return df.drop(columns=df.columns.difference(cols))


def drop_columns(df: pd.DataFrame, cols: List[str]) -> pd.DataFrame:
    return df.drop(columns=cols)


def set_index(df: pd.DataFrame, cols: Optional[List[str]] = None) -> pd.DataFrame:
    if cols is None:
        return df.reset_index()
    return df.set_index(cols)


def dedupe(df: pd.DataFrame, cols: Optional[List[str]] = None) -> pd.DataFrame:
    if cols is None:
        return df[~df.index.duplicated(keep="last")]
    return df.drop_duplicates(subset=cols, keep="last")


def sort(df: pd.DataFrame, cols: List[str]) -> pd.DataFrame:
    return df.sort_values(by=cols)


def save_df_to_csv(df: pd.DataFrame, to_path: Path) -> None:
    logger.info(f"Creating file {to_path}")
    to_path.parent.mkdir(parents=True, exist_ok=True)
    df.to_csv(to_path, index=False)


def load_df_from_csv(from_path: Path) -> pd.DataFrame:
    return pd.read_csv(from_path)


def save_df_to_json(df: pd.DataFrame, to_path: Path) -> None:
    logger.info(f"Creating file {to_path}")
    to_path.parent.mkdir(parents=True, exist_ok=True)
    df.to_json(to_path, orient="records", indent=4)


def get_current_time() -> datetime:
    return datetime.now(tz=timezone.utc)


def get_download_time_path_full_pdf(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "full" / f"{month}_timestamp.json"


def get_download_time_path_full_csv(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "fullcsv" / f"{month}_timestamp.json"


def get_download_time_path_sale(data_dir: Path, month: Month) -> Path:
    return data_dir / "downloaded" / "sale" / f"{month}_timestamp.json"


def get_parse_time_path_full(data_dir: Path, month: Month) -> Path:
    return data_dir / "parsed" / "full" / f"{month}_timestamp.json"


def get_parse_time_path_sale(data_dir: Path, month: Month) -> Path:
    return data_dir / "parsed" / "sale" / f"{month}_timestamp.json"


def write_timestamp(timestamp: datetime, to_path: Path) -> None:
    logger.info(f"Saving timestamp {timestamp} to {to_path}")
    to_path.parent.mkdir(parents=True, exist_ok=True)

    obj = {
        "timestamp": timestamp.timestamp(),
        "iso8601": timestamp.isoformat(),
    }

    with open(to_path, mode="w") as fh:
        json.dump(obj, fh, indent=4)


def read_timestamp(from_path: Path) -> datetime:
    with open(from_path, mode="r") as fh:
        obj = json.load(fh)
    return datetime.fromtimestamp(obj["timestamp"], tz=timezone.utc)


T = TypeVar("T")


def find_max(x: Sequence[T]) -> T:
    return max(x)


def is_greater_than(x: T, y: T) -> bool:
    """
    Return true if `x` is greater than `y`.
    """

    return x > y
